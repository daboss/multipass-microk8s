#! /bin/sh

# configurations
RELEASE=20.04

CPU_MAIN=2
CPU_WORK=1

RAM_MAIN=2G
RAM_WORK=2G

DISK_MAIN=20G
DISK_WORK=20G

VERBOSITY=vvv

# definitions
MAIN=mk8s
WORK_1=mk8s-w1
WORK_2=mk8s-w2

WORKER_NODES="$WORK_1 $WORK_2"
CLUSTER_NODES="$MAIN $WORKER_NODES"

AUTHORIZED_KEY="not set"

PUB_KEY_FILE=~/.ssh/id_rsa.pub

if [ -f $PUB_KEY_FILE ]
then
	AUTHORIZED_KEY=$(cat $PUB_KEY_FILE)
fi

CLOUD_INIT=cloud-init.yaml

tee $CLOUD_INIT << EOF
---
users:
  - name: ubuntu
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh-authorized-keys:
      - $AUTHORIZED_KEY

apt_update: true
apt_upgrade: true
# apt_reboot_if_required: true
snap:
    commands:
      00: snap install microk8s --channel=1.23/stable --classic
      01: usermod -a -G microk8s ubuntu
#      02: microk8s disable ha-cluster --force
---

EOF

function createNode() {
	NAME=$1
	CPUS=$2
	RAM=$3
	DISK=$4
	echo
	time multipass launch -$VERBOSITY \
		-c $CPUS \
		-m $RAM \
		-d $DISK \
		-n $NAME \
		$RELEASE \
		--cloud-init $CLOUD_INIT 
}

function joinCluster() {
	NAME=$1
	JOIN="$(multipass exec $MAIN -- microk8s add-node | grep microk8s | head -n 1) --worker"
	echo
	multipass exec -$VERBOSITY $NAME -- $JOIN
}

for NODE in $CLUSTER_NODES
do
	time multipass delete -$VERBOSITY $NODE &
done

wait
multipass purge

createNode $MAIN $CPU_MAIN $RAM_MAIN $DISK_MAIN 
for NODE in $WORKER_NODES
do
	createNode $NODE $CPU_WORK $RAM_WORK $DISK_WORK 
done

rm $CLOUD_INIT

for NODE in $WORKER_NODES
do
	joinCluster $NODE
done


echo
#multipass exec $MAIN -- microk8s disable ha-cluster
multipass exec $MAIN -- microk8s kubectl get nodes

